(function() {
	$('#datetimepicker1').datetimepicker({
		widgetPositioning: {
				vertical: 'bottom'
		},
		format: 'MMMM D',
		dayViewHeaderFormat: 'MMMM',
	});

	$('#datetimepicker2').datetimepicker({
		widgetPositioning: {
			vertical: 'bottom'
		},
		format: 'h:mm a',
		// debug: true,
	});

})();

var $s = $('#mu-id');
