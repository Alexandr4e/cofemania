$(document).ready(function() {
	$('body').addClass('loaded');

	var mySwiper = new Swiper ('#slider-one', {
		// Optional parameters
		loop: true,

		// If we need pagination
		pagination: {
			el: '.slider__pagination',
		},

		// Navigation arrows
		navigation: {
			nextEl: '.slider__button-next',
			prevEl: '.slider__button-prev',
		},
	});

	//= parts/01_form.js
	//= parts/02_mobile-menu.js

});
